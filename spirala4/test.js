let assert = chai.assert;
describe('Test', function() {
        describe('icrtajVjezbu(0,[ ])', function() {
                it('should draw 0 execise', function() { //provjeravamo broj vjezbi
                        Vjezba.iscrtajVjezbu(document.getElementById("vjezbe"),{brojVjezbi: 0,brojZadatakaPoVjezbi:[]});
                        let vjezbe = document.getElementsByTagName("br");
                        assert.equal(vjezbe.length, 0,"Broj vježbi treba biti 0");
                });
                it('no tasks', function() { //svaka vjezba treba imati po 2 zadatka
                        let zadatak = document.getElementsByTagName("p");
                        assert.equal(zadatak.length, 0,"Nema zadataka");
                });
                it('(in)visibility of tasks', function(){ // provjeravamo da li su testovi nevidljivi
                        let zadatak =  document.getElementsByTagName("p");
                        let brojac = 0;
                        for( let i=0;i<zadatak.length; i++){
                              let stil=window.getComputedStyle(zadatak[i]);
                              if(stil.display!=='none')  brojac++;
                        }
                        assert.equal(brojac, 0, "Nijedan zadatak ne treba biti vidljiv");
                })
        });
        describe('icrtajVjezbu(5,[2,2,2,2,2])', function() {
                it('should draw 5 execise', function() { //provjeravamo broj vjezbi
                        Vjezba.iscrtajVjezbu(document.getElementById("vjezbe"),{brojVjezbi: 5,brojZadatakaPoVjezbi: [2,2,2,2,2]});
                        let vjezbe = document.getElementsByTagName("br");
                        assert.equal(vjezbe.length, 5,"Broj vježbi treba biti 5");
                });
                it('each exercise should have two tasks', function() { //svaka vjezba treba imati po 2 zadatka
                        let zadatak = document.getElementsByTagName("p");
                        assert.equal(zadatak.length, 10,"Svaka vježba treba imati po 2 zadataka");
                });
                it('(in)visibility of tasks', function(){ // provjeravamo da li su testovi nevidljivi
                        let zadatak =  document.getElementsByTagName("p");
                        let brojac = 0;
                        for( let i=0;i<zadatak.length; i++){
                              let stil=window.getComputedStyle(zadatak[i]);
                              if(stil.display!=='none')  brojac++;
                        }
                        assert.equal(brojac, 0, "Nijedan zadatak ne treba biti vidljiv");
                })
        });
        describe('iscrtajVjezbu(16,[15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15])', function() { //granicni slucaj
                it('should draw 15 execise', function(){
                        Vjezba.iscrtajVjezbu(document.getElementById("vjezbe"),{brojVjezbi: 16,brojZadatakaPoVjezbi: [15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15]});
                        let vjezbe = document.getElementsByTagName("br");
                        assert.equal(vjezbe.length, 20,"Broj vježbi treba biti 15"); //20 jer se sve dodaje u isti div
                });
                it('each exercise should have 15 tasks', function(){
                        let zadatak=document.getElementsByTagName("p");
                        assert.equal(zadatak.length, 235,"Svaka vježba treba imati po 15 zadataka"); //235 jer prve pet vjezbih imaju vec zadatke
                });
                it('(in)visibility of tasks', function(){ 
                        let zadatak =  document.getElementsByTagName("p");
                        let brojac = 0;
                        for( let i=0;i<zadatak.length; i++){
                              let stil=window.getComputedStyle(zadatak[i]);
                              if(stil.display!=='none')  brojac++;
                        }
                        assert.equal(brojac, 0, "Nijedan zadatak ne treba biti vidljiv");
                });
        });
        describe('isrctajVjeznu(1,[16])', function(){ //granicni slucaj za zadatak
                it('should draw 1 exercise', function(){
                        Vjezba.iscrtajVjezbu(document.getElementById("vjezbe"),{brojVjezbi: 1,brojZadatakaPoVjezbi: [16]});
                        let vjezbe=document.getElementsByTagName("br");
                        assert.equal(vjezbe.length, 21,"Treba biti samo jedna vježba"); //21 jer se racunaju i prije dodane vjezbe
                });
                it('exercise should have 15 tasks', function(){
                        let zadatak=document.getElementsByTagName("p");
                        assert.equal(zadatak.length, 250,"Vježba treba imati 15 zadataka"); //250 jer se racunaju od prije zadaci
                });
                it('(in)visibility of tasks', function(){ 
                        let zadatak =  document.getElementsByTagName("p");
                        let brojac = 0;
                        for( let i=0;i<zadatak.length; i++){
                              let stil=window.getComputedStyle(zadatak[i]);
                              if(stil.display!=='none')  brojac++;
                        }
                        assert.equal(brojac, 0, "Nijedan zadatak ne treba biti vidljiv");
                });
        });
        describe('iscrtajVjezbu(8, [3,8,6,2,11,1,5,14])', function(){
                it('should draw 8 exercise', function(){
                        Vjezba.iscrtajVjezbu(document.getElementById("vjezbe"),{brojVjezbi: 8,brojZadatakaPoVjezbi: [3,8,6,2,11,1,5,14]});
                        let vjezbe=document.getElementsByTagName("br");
                        assert.equal(vjezbe.length, 29,"Broj vježbi treba biti 8"); //29 jer zbajaju se sve do sada vjezbe
                });
                it('exercise should have 15 tasks', function(){
                        let zadatak=document.getElementsByTagName("p");
                        assert.equal(zadatak.length, 300,"Broj zadataka u vježbama nije uredu"); //300 jer se zbrajaju svi zadaci zadaci
                });
                it('(in)visibility of tasks', function(){ 
                        let zadatak =  document.getElementsByTagName("p");
                        let brojac = 0;
                        for( let i=0;i<zadatak.length; i++){
                              let stil=window.getComputedStyle(zadatak[i]);
                              if(stil.display!=='none')  brojac++;
                        }
                        assert.equal(brojac, 0, "Nijedan zadatak ne treba biti vidljiv");
                });
        });
});